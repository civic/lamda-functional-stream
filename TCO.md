再帰の最適化
=====================================-

階乗計算メソッドfactorialRecを持ったクラスを実装し、大きい数でStackOverFlowが発生することを確認する。

## 最適化前の階乗計算
```java
public class Factorial {
    public static int factorialRec(final int number){
        if (number == 1) {
            return number;
        } else {
            return number * factorialRec(number -1);
        }
    }

    public static void main(String[] args) {
        System.out.println(factorialRec(5));
        System.out.println(factorialRec(20000));    //Stack over flow
    }

}
```

## 再帰をすぐさま実行せずに定義を行うだけに留める実装

- 末尾再帰するための TailCall クラスの定義。
- TailCallを返すfactorialRec関数。
- resultを取得しようとするとどうなるか？

## Streamを使って末尾再帰うんぬん...
