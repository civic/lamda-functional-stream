ラムダ、関数型インターフェース、Stream
============================================

JavaでStreamやラムダを自然に使えるようになるためのドリル。

そして関数型プログラミングへ〜


# ラムダ式/関数型インターフェース

<http://www.ne.jp/asahi/hishidama/home/tech/java/lambda.html>



- Javaには関数オブジェクトが存在しない 
    - すべてクラスのメソッド
    - Callableなオブジェクトみたいなのはない
    - →関数型インターフェース
- Callableなオブジェクトを表すためのインターフェース
    - 抽象メソッドが1つだけ
    - staticメソッドやデフォルトメソッドは含まれていてもOK


## 標準的な関数型インターフェース一覧

|  戻値<br/>＼<br/>引数|void|boolean|R|T(引数と同じ型)|int|
|------------|----|-------|---|---------------|---|
|`なし`|`Runnable`|`BooleanSupplier`|`Supplier<R>`|`-`|`IntSupplier`|
|`T`|`Consumer<T>`|`Predicate<T>`|`Function<T,R>`|`UnaryOperator<T>`|`ToIntFunction<T>`|
|`int`|`IntConsumer`|`IntPredicate`|`IntFunction<R>`|`-`|`IntUnaryOperator`|
|`long`|`LongConsumer`|`LongPredicate`|`LongFunction<R>`|`-`|`LongToIntFunction`|
|`double`|`DoubleConsumer`|`DoublePredicate`|`DoubleFunction<R>`|`-`|`DoubleToIntFunction`|
|`T, U`|`BiConsumer<T,U>`|`BiPredicate<T,U>`|`BiFunction<T,U,R>`|`-`|`ToIntBiFunction<T,U>`|
|`T, T`|`-`|`-`|`-`|`BinaryOperator<T>`|`-`|
|`T, int`|`ObjIntConsumer`|`-`|`-`|`-`|`-`|
|`T, long`|`ObjeLongConsumer`|`-`|`-`|`-`|`-`|
|`T, double`|`ObjDoubleConsumer`|`-`|`-`|`-`|`-`|
|`int int`|`-`|`-`|`-`|`-`|`IntBinaryOperator`|

### 覚え方
関数型インターフェースを使う場合は、ラムダ式で記述すればあまり覚えてなくても使えるが、一応簡単な覚え方。

### Supplier系
なにも引数を与えずとも、無から生み出してくれる供給マシーン。

### Consumer系
引数を受け取って消化してしまう恐ろしい怪獣のような存在。

### Predicate系
受け取った引数をなんでもYESかNOで仕分けしてしまうジャッジマン。

### Function系
引数Tを受け取ってRを返す。  
※ただし、Rがbooleanの場合は`Predicate<T>`  
※ただし、引数TとRが同じ型→`〜Operator<T>`  
※引数2つ系→`〜BiFunction<T, U, R>`

# Stream

大きく分けて3段階の処理の流れ。

1. Streamの生成
2. 中間処理
3. 終端処理

生成→中間処理→中間処理... →終端処理

終端処理を記述するまで中間処理は行われない。一度終端処理を行うと、以後は中間処理は行えない。
（ただし終端処理でStreamを生成すると別Streamとして中間処理はできる）

# 実践ドリル

- ラムダ、関数型インターフェス
    - 1) Runnableなlambda式を作成せよ
    - 2) Supplierなlambda式を作成せよ
    - 3) Consumerなlambda式を作成せよ
    - 4) Predicateなlambda式を作成せよ
    - 5) Functionなlambda式を作成せよ
    - 6) id生成処理を外部から関数で指定できるように書きなおせ
    - 7) Comparatorを指定したsortをラムダ式で作成せよ
    - 8) ラムダ式で遅延生成するように修正せよ
    - 9) 複数の関数を結合した関数を作成せよ
    - 10) メソッド参照で関数を渡す処理を記述せよ
- Stream生成
    - 11) 可変長引数からStreamを生成せよ
    - 12) 配列からStreamを生成せよ
    - 13) IntStreamを範囲指定(0以上10未満/ 0以上10以下)
    - 14) 無限に10を生成するStreamを生成せよ
    - 15) 1,11,21 ..と無限に増えていくStreamを生成せよ
    - 16) テキストファイルの各行を返すStreamを生成せよ
- Stream中間処理
    - 17) Stream#filterを使った中間処理をせよ
    - 18) Stream#mapを使った中間処理をせよ
    - 19) Stream#flatMap, distinct, sortを使った中間処理をせよ
    - 20) Stream#peekを使った中間処理をせよ
    - 21) Stream#limit, skipをつかった中間処理をせよ
- Stream終端処理
    - 22) Stream#toArrayを使ってStreamからArrayを生成せよ
    - 23) Stream#maxを使って最大の要素を選択せよ
    - 24) Stream#countを使って3の倍数になる要素の個数をカウントせよ
    - 25) Stream#findFirstを使って条件に一致する最初の要素を検索せよ
    - 26) Stream#allMatchを使ってすべての要素が条件に一致するかチェックせよ
    - 27) Stream#reduceを使って合計を算出せよ（あえてsumは使わない）
    - 28) Stream#collectとCollectors#summingIntを使って合計を算出せよ
    - 29) Stream#collectとCollectors#groupingByを使ってグループごとに要素を分割、グループごとの合計を算出せよ



