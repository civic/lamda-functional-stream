package practice_intermediate;

import java.util.stream.IntStream;

/**
 * 無限Streamを、5飛ばして6件目から10回出力するように修正せよ。
 */
public class P21LimitSkip {
    public static void main(String[] args) {

        IntStream.iterate(0, i -> i + 1)
                .forEach(System.out::println);
    }
}
