package practice_intermediate;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;

/**
 * Streamの操作に修正せよ。
 * ユーザの所属グループの一覧を重複なしで辞書順で出力。
 * @see Stream#flatMap(java.util.function.Function) 
 * @see Stream#distinct() 
 * @see Stream#sorted() 
 */
public class P19FlatMapDistinctSorted {

    public static void main(String[] args) {
        
        List<User> users = Arrays.asList(
                new User("Rimper", "admin", "manager"),     //ユーザ名Rimper,所属グループadmin, manager
                new User("Vesess", "guest"), 
                new User("Melt", "manager", "backup"), 
                new User("Aleskelves", "staff"), 
                new User("Verny", "service", "backup"));


        Set<String> uniqueGroups = new TreeSet<>(); //Setは重複のない集合, TreeSetはソート済みSet
        for(User user: users){
            for(String group: user.groups){
                uniqueGroups.add(group);
            } 
        }

        for(String group: uniqueGroups){
            System.out.println(group);  //admin,backup,guest,manager,service,staff
        }
    }

    private static class User{
        public String name;
        public String[] groups;

        public User(String name, String ... groups) {
            this.name = name;
            this.groups = groups;
        }


    }
}
