package practice_intermediate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Stream操作に修正せよ。
 * 各nameのlengthを出力
 * @see Stream#map(java.util.function.Function) 
 */
public class P18Map {

    public static void main(String[] args) {
        
        List<String> names = Arrays.asList("Rimper", "Vesess", "Melt", "Aleskelves", "Verny", "Doryn", "Perryn",
                "Engrny", "Ildchair", "Mortin", "Awes", "Heuntgha", "Kinightust", "Tanem");
        
        for (String name: names){
            System.out.println(name.length());
        }

    }
}
