package practice_intermediate;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Stream;

/**
 * 2回のフィルターと、map処理を行うStreamに対して、各中間処理で中間結果を出力せよ。
 * @see Stream#peek(java.util.function.Consumer) 
 */
public class P20Peek {
    public static void main(String[] args) {
        
        List<User> users = Arrays.asList(
                new User("Rimper", "admin", "manager", "backup"),     //ユーザ名Rimper,所属グループadmin, manager,backup
                new User("Vesess", "guest"), 
                new User("Melt", "manager"), 
                new User("Aleskelves", "staff", "backup"), 
                new User("Verny", "service", "backup"));

        //backupグループに所蔵し、nameに"r"が含まれるUserを出力
        users.stream()
                .filter(u -> u.isGroupOf("backup"))
                //このフィルターの段階でのnameを出力
                .filter(u -> u.name.contains("r"))
                .map(u -> u.name)
                .forEach(System.out::println);
    }

    private static class User{
        public String name;
        public String[] groups;

        public User(String name, String ... groups) {
            this.name = name;
            this.groups = groups;
        }

        public boolean isGroupOf(String group){
            return new HashSet<>(Arrays.asList(groups)).contains(group);
        }

    }
}
