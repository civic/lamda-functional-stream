package practice_intermediate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Stream操作に修正せよ。
 * @see Stream#filter(java.util.function.Predicate) 
 */
public class P17Filter {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Rimper", "Vesess", "Melt", "Aleskelves", "Verny", "Doryn", "Perryn",
                "Engrny", "Ildchair", "Mortin", "Awes", "Heuntgha", "Kinightust", "Tanem");
        
        for (String name: names){
            if (name.startsWith("A")){
                System.out.println(name);
            }
        }
    }

}
