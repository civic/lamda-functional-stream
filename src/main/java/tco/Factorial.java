package tco;

import java.math.BigInteger;

/**
 * 再帰での階乗計算。
 */
public class Factorial {
    
    public static BigInteger factorialRec(final int number){
        BigInteger num = BigInteger.valueOf(number);

        if (number == 1) {
            return num;
        } else {
            return num.multiply(factorialRec(number - 1));
        }
    }

    public static void main(String[] args) {
        System.out.println(factorialRec(5));
        System.out.println(factorialRec(20000));    //Stack Overflow
    }

}
