package tco;

import java.math.BigInteger;
import java.util.stream.Stream;

/**
 * 階乗計算の最適化。
 */
public class Factorial3 {
    @FunctionalInterface
    public interface TailCall {
        TailCall apply();

        default BigInteger result(){
            return null;
        }
    }

    public static TailCall factorialRec(final BigInteger factorial, final int number){
        BigInteger bignum = BigInteger.valueOf(number);

        /** Factorial2 で書いた処理 */

        return null;
    }

    public static BigInteger factorial(final int number){
        // Streamをつかってうまい具合に 
        return null;
    }

    public static void main(String[] args) {
        System.out.println(factorial(5));
        System.out.println(factorial(20000));    //Stack Overflowしない

    }

}
