package tco;

import java.math.BigInteger;

/**
 * 階乗計算の最適化。
 */
public class Factorial2 {
    @FunctionalInterface
    public interface TailCall {
        TailCall apply();

        default BigInteger result(){
            return null;
        }
    }

    public static TailCall factorialRec(final BigInteger factorial, final int number){
        BigInteger bignum = BigInteger.valueOf(number);

        //
        // なんらかの処理
        //

        return null;

    }

    public static void main(String[] args) {
        /* 
         * このFactorialRecはStack Overflowしない
         * なぜか？
         * factorialRecは階乗を算出するためのTailCallオブジェクトを返すだけで
         * その時点では再帰呼び出しをしていない。applyを実行するとn-1の階乗を計算するためのTailCallを返す。
         * 
         * applyを呼びつづけて、1のTailCallまで取得したあとにresultを呼ぶと、最終的に求める算出値が得られる。
         * 
         */
        System.out.println(factorialRec(BigInteger.ONE, 5));
        System.out.println(factorialRec(BigInteger.ONE, 20000));    //Stack Overflowしない

        System.out.println(factorialRec(BigInteger.ONE, 5)
                .apply().apply().apply().apply().result());

        //System.out.println(factorialRec(BigInteger.ONE, 20000)    
        //        .apply(). .......... .apply().result());          //20000の階乗についても同様だが?
    }

}
