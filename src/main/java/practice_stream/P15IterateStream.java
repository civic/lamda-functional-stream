package practice_stream;

import java.util.stream.IntStream;

/**
 * 1,11,21,...で増えていく無限Stream。
 */
public class P15IterateStream {
    public static void main(String[] args) {

        IntStream stream = null;   

        stream.limit(10).forEach(System.out::println);
    }

}
