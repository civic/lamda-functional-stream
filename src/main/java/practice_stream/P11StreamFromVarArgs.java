package practice_stream;

import java.util.stream.Stream;

/**
 * 可変長引数からStreamを生成
 * @see Stream
 */
public class P11StreamFromVarArgs {
    
    public static void main(String[] args) {

        Stream<String> stream = null;//hogehoge("aa", "bb", "cc", ....);
        
        stream.forEach(System.out::println);
    }
}
