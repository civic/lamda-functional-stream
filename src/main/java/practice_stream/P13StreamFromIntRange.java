package practice_stream;

import java.util.stream.IntStream;

/**
 * 0以上、10未満のIntStreamの生成
 * 0以上、10以下のIntStreamの生成
 * @see IntStream
 */
public class P13StreamFromIntRange {
    
    public static void main(String[] args) {
        IntStream stream1 = IntStream.of(0, 1 ,2, 3, 4, 5 ,6, 7, 8, 9);
        IntStream stream2 = IntStream.of(0, 1 ,2, 3, 4, 5 ,6, 7, 8, 9, 10);
        
        stream1.forEach(System.out::print);
        System.out.println();
        stream2.forEach(System.out::print);

        
    }
}
