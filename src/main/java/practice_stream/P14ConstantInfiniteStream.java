package practice_stream;

import java.util.stream.IntStream;

/**
 * 無限に10を出力するIntStreamを生成。
 * 
 * @see IntStream
 */
public class P14ConstantInfiniteStream {

    public static void main(String[] args) {
        IntStream stream = null;

        stream.limit(10).forEach(System.out::println);  //無限Streamだが10回でStop
    }
}
