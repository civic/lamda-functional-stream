package practice_stream;

import java.io.IOException;
import java.nio.file.Files;
import java.util.stream.Stream;

/**
 * ファイルの各行の文字列を返すStreamの生成
 * @see Files
 */
public class P16FileLinesStream {
    public static void main(String[] args) throws IOException {
        Stream<String> stream = null;   //ファイルの各行を返す;

        stream.forEach(System.out::println);
        

    }

}
