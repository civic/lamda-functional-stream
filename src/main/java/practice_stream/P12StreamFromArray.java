package practice_stream;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * 配列からStreamを生成
 * @see Arrays
 */
public class P12StreamFromArray {
    
    public static void main(String[] args) {
        String[] strings = {"AAA", "BBB", "CCC"};

        Stream<String> stream = null;
        
        stream.forEach(System.out::println);
    }
}
