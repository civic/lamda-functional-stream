package practice_terminate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Productの価格のトータルを、mapToInt,reduceで算出せよ。
 * @see Stream#mapToInt( (java.util.function.Function) 
 * @see Stream#reduce(java.util.function.BinaryOperator) 
 */
public class P27Reduce {

    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
                new Product(95), 
                new Product(120), 
                new Product(280), 
                new Product(80)
        );

        int totalPrice = 0;

        for (Product product: products){
            totalPrice += product.price;
        }

        System.out.println("total=" + totalPrice);

    }
    static class Product{
        int price;
        public Product(int price) { this.price = price; }
    }

}
