
package practice_terminate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Streamのすべての要素が4字以上であるかどうかをチェックする処理をStream操作で書き直せ。
 * @see Stream#allMatch(java.util.function.Predicate) 
 */
public class P26AllMatch {

    public static void main(String[] args) {
        
        List<String> names = Arrays.asList("Rimper", "Vesess", "Melt", "Aleskelves", "Verny", "Doryn", "Perryn",
                "Engrny", "Ildchair", "Mortin", "Awes", "Heuntgha", "Kinightust", "Tanem");

        boolean allMatch = true;

        for (String name: names){
            if (name.length() < 4){
                allMatch = false;
                break;
            }
        }

        System.out.println(allMatch);
    }
}
