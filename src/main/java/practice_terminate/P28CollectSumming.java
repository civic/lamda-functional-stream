package practice_terminate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Productの価格のトータルを、collectで算出せよ。
 * @see Stream#collect(java.util.stream.Collector) 
 * @see Collectors#summingInt(java.util.function.ToIntFunction)
 */
public class P28CollectSumming {

    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
                new Product(95), 
                new Product(120), 
                new Product(280), 
                new Product(80)
        );

        int totalPrice = 0;

        for (Product product: products){
            totalPrice += product.price;
        }

        System.out.println("total=" + totalPrice);

    }
    static class Product{
        int price;
        public Product(int price) { this.price = price; }
    }
}
