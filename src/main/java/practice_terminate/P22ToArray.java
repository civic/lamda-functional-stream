
package practice_terminate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Streamの中間操作のあとに、Arrayを生成している処理を書き直せ.
 * @see Stream#toArray() 
 */
public class P22ToArray {
    public static void main(String[] args) {
        
        List<String> names = Arrays.asList("Rimper", "Vesess", "Melt", "Aleskelves", "Verny", "Doryn", "Perryn",
                "Engrny", "Ildchair", "Mortin", "Awes", "Heuntgha", "Kinightust", "Tanem");

        //"A"か"V"で始まるnameの配列を作成したい
        List<String> namesAorVList = new ArrayList<>();

        names.stream()
                .filter(n -> n.startsWith("A") || n.startsWith("V"))
                .forEach(n -> namesAorVList.add(n));

        String[] namesAorVArray = new String[namesAorVList.size()];
        namesAorVList.toArray(namesAorVArray);

        System.out.println(Arrays.asList(namesAorVArray));
    }

}
