package practice_terminate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * ProductはグループAの製品、グループBの製品に分類できる。
 * グループごとにProductのListを保持するMapを作成せよ。
 * また、グループごとにProductの価格トータルを保持するMapを作成せよ。
 * @see Stream#collect(java.util.stream.Collector) 
 * @see Collectors#groupingBy(java.util.function.Function) 
 * @see Collectors#groupingBy(java.util.function.Function, java.util.stream.Collector) 
 * @see Collectors#summingInt(java.util.function.ToIntFunction) 
 */
public class P29CollectGrouping {

    public static void main(String[] args) {
        List<Product> allProducts = Arrays.asList(
                new Product(95, "A"), 
                new Product(120, "B"), 
                new Product(280, "A"), 
                new Product(80, "B")
        );

        Map<String, List<Product>> groupProductsMap = new HashMap<>();

        for (Product product: allProducts){
            List<Product> products = groupProductsMap.computeIfAbsent(product.group, s->new ArrayList<>());
            products.add(product);
        }

        System.out.println(groupProductsMap);

        Map<String, Integer> groupTotalMap = new HashMap<>();
        for (Product product: allProducts){
            Integer groupTotal = groupTotalMap.computeIfAbsent(product.group, s->0);
            groupTotalMap.put(product.group, groupTotal + product.price);
        }
        System.out.println(groupTotalMap);

    }
    static class Product{
        int price;
        String group;
        public Product(int price, String group) {
            this.price = price;
            this.group = group;
        }

        @Override
        public String toString() {
            return String.format("(group=%s:price=%d)", this.group, this.price);
        }
        
    }
}
