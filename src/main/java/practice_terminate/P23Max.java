package practice_terminate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Productの集合から最も高価なProductを選択する処理をStreamを使って書き直せ。
 * 集合が空だった場合はどうなるか考えよ。
 * @see Stream#max(java.util.Comparator) 
 */
public class P23Max {

    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
                new Product(100), 
                new Product(120), 
                new Product(280), 
                new Product(80)
        );

        Product mostExpensiveProduct = null;
        for (Product product: products){
            if (mostExpensiveProduct == null){
                mostExpensiveProduct = product;
            } else if (mostExpensiveProduct.price < product.price){
                mostExpensiveProduct = product;
            }
        }

        System.out.println("Most expensive product: " + mostExpensiveProduct.price);
    }
    static class Product{
        int price;
        public Product(int price) { this.price = price; }
    }
}
