
package practice_terminate;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * StremからVで始まる名前で最初に見つかるものを検索せよ。
 * @see Stream#findFirst() 
 * @see Optional
 */
public class P25FindFirst {
    public static void main(String[] args) {
        
        List<String> names = Arrays.asList("Rimper", "Vesess", "Melt", "Aleskelves", "Verny", "Doryn", "Perryn",
                "Engrny", "Ildchair", "Mortin", "Awes", "Heuntgha", "Kinightust", "Tanem");

        String foundName = null;

        for (String name: names){
            if (name.startsWith("V")){
                foundName = name;
                break;
            }
        }

        if (foundName != null){
            System.out.println(foundName);
        }
    }

}
