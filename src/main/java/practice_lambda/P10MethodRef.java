package practice_lambda;

import java.util.function.Consumer;

/**
 * メソッド参照に修正せよ。
 */
public class P10MethodRef {
    public static void main(String[] args) {
        
        Consumer<String> consumer = new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        };
        consumer.accept("Hello World");

    }

}
