package practice_lambda;

import java.util.Random;

/**
 * DataLoaderクラスは、指定されたデータをインポートするような機能を持つ。
 * DataLoaderの内部でid生成処理が実装されているが、DataLoaderを使用する側から指定可能なように変更せよ。
 * DataLoaderのコンストラクタでid生成のアルゴリズムを渡せるように。
 */
public class P06Delegate {
    
    public static void main(String[] args) {
        DataLoader loader = new DataLoader();
        loader.bulkInsert("scott", "john", "taro");

    }
    static class DataLoader{
        public void bulkInsert(String ... names){
            for (String name: names){
                String id = createNewId();
                System.out.printf("INSERT: %5s(%s)\n", name, id);
            }
        }
        private String createNewId() {
            return "id-" +  Math.abs(new Random().nextInt());
        }

    }
}
