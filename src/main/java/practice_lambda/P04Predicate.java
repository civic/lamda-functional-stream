package practice_lambda;

import java.util.Arrays;
import java.util.function.Predicate;

/**
 * ラムダ式にせよ
 */
public class P04Predicate {
    
    public static void main(String[] args) {
        
        Predicate<Integer> isExpensive = new Predicate<Integer>() {
            @Override
            public boolean test(Integer t) {
                return t > 100;
            }
        };

        for (Integer price: Arrays.asList(10, 90, 101, 120, 35, 60, 900)){
            if (isExpensive.test(price)){
                System.out.println(price);
            }
        }
    }
}
