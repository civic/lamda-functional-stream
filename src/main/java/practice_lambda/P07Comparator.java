package practice_lambda;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class P07Comparator {
    /**
     * 文字列リストを独自の条件でソートするためのComparatorを実装せよ。
     * 1文字目を無視した2文字目以降の辞書順でソートする。
     */
    public static void main(String[] args) {
        List<String> strings = Arrays.asList("c04", "a02", "b01", "d03");

        Collections.sort(strings);  //[a02, b01, c04, d03]
                                    // 修正後は [b01, a02, d03, c04]

        System.out.println(strings);
    }
    
}
