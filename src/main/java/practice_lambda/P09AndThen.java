package practice_lambda;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * Printerクラスは渡されたProductの価格改定表を出力する。
 * 改定後の価格はFunctionで渡された関数にしたがって算出する。
 * 既存のdiscount値の算出後に各商品に対して送料$100を加算した価格改定表を出力せよ。
 * ただしdiscountをそのように修正するのではなく、値引き後に送料を加算する計算をする関数を作り出す。
 * @see Function#andThen(java.util.function.Function) 
 */
public class P09AndThen {
    
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product(2500), new Product(1000), new Product(3000));

        Function<Integer, Integer> discount = price -> (int)(price * 0.9);


        new Printer().print(products, discount);

    }

    static class Product{
        int price;
        public Product(int price) { this.price = price; }
    }

    static class Printer{
        /**
         * 価格変換関数を使って、価格改定表を出力する
         */
        public void print(List<Product> products, Function<Integer, Integer> calc){
            for (Product p : products){
                System.out.printf("Product: $%,d -> $%,d\n", 
                    p.price, calc.apply(p.price));
            }
        }
    }

}
