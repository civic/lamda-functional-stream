package practice_lambda;

/**
 * ラムダ式を使用せよ
 */
public class P01Runnable {
    public static void main(String[] args) {
        System.out.println("thread name: " + Thread.currentThread().getName());
        
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("  thread name: " + Thread.currentThread().getName());
            }
        });
        th.start();
        
    }
    
}
