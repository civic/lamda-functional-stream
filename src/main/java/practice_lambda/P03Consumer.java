package practice_lambda;

import java.util.function.Consumer;

/**
 * ラムダ式にせよ
 */
public class P03Consumer {
    public static void main(String[] args) {
        Consumer<String> printer = new Consumer<String>() {
            @Override
            public void accept(String t) {
                System.out.println(">>> " + t);
            }
        };

        printer.accept("hello");
        
    }
}
