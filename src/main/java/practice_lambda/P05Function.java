package practice_lambda;

import java.util.function.Function;

/**
 * ラムダ式にせよ
 */
public class P05Function {
    
    public static void main(String[] args) {
        Function<Integer, String> numberString = new Function<Integer, String>() {
            @Override
            public String apply(Integer t) {
                return "[" + t + "]";
            }
        };

        System.out.println(numberString.apply(10));
    }

}
