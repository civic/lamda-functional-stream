package practice_lambda;

import java.util.function.Supplier;

/**
 * ラムダ式にせよ
 */
public class P02Supplier {
    public static void main(String[] args) {
        
        Supplier<String> idFactory = new Supplier<String>() {
            @Override
            public String get() {
                return "id-" + System.currentTimeMillis();
            }
        };

        String uid = idFactory.get();
        System.out.println(uid);
    }
}
