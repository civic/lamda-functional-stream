
package practice_lambda;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * 生成に時間のかかる重いオブジェクトがログ出力に使用されている。
 * ログレベルOFFの場合はログ出力されなくてもHeavyインスタンスが生成されてしまうので、
 * 遅延生成することでHeavyインスタンスが生成されないように修正せよ。
 * 
 * @see Logger#log(java.util.logging.Level, java.util.function.Supplier) 
 */
public class P08LazyInstansiation {

    public static void main(String[] args) throws Exception {
        Logger logger = createLogger();

        //logger.setLevel(Level.INFO);
        logger.setLevel(Level.OFF);

        logger.log(Level.INFO, "heavy=" + new Heavy());

    }

    private static class Heavy {
        public Heavy() {
            System.out.println("!!! Heavy instance created !!!");
        }

        @Override
        public String toString() {
            return "(heavy instance)";
        }
        
    }

    public static Logger createLogger() throws SecurityException {
        Logger logger = Logger.getAnonymousLogger();
        logger.setUseParentHandlers(false);
        logger.addHandler(new ConsoleHandler());
        return logger;
    }

}

